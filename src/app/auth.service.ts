import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>; 
  




  
  //SignUp(email:string, password:string){
  
   // this.afAuth.createUserWithEmailAndPassword(email,password).then(
   //   res =>{
     //   console.log('Succesful login;');
      //  this.router.navigate(['/books']); 
     // }
   // ).catch(function(error) {
      // Handle Errors here.
     // var errorCode = error.code;
    //  var errorMessage = error.message;
     // if (errorCode == 'auth/email-already-in-use') {
        //alert('email-already-in-use');
     // } else {
      ///  alert(errorMessage);
     // }
      
     // console.log(error);
   // });
 // }

 SignUp(email:string, password:string){
  return this.afAuth.createUserWithEmailAndPassword(email,password);
}

  


  
  
  login(email:string, password:string){
   return this.afAuth.signInWithEmailAndPassword(email,password);
        //.then(res => {
       //   console.log(res);
      //    this.router.navigate(['/books']); 
     //  }
  //    )
 }

  logout(){
    this.afAuth.signOut(); 
  }

  getUser():Observable<User | null>  {
    return this.user; 
  }

  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState; 
  }
}
