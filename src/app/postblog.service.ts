import { PostblogRaw } from './interfaces/postblog-raw';
import { Postblog } from './interfaces/postblog';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/internal/operators/map';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostblogService {

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  
  constructor(private http:HttpClient ) {}

  getPosts():Observable<Postblog>{
    return this.http.get<PostblogRaw>(this.URL).pipe(
     map(posts => posts.map(post => this.transformPostsData(post))),
      catchError(this.handleError)
    )
  }
   
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error');
  }

  private transformPostsData(post:PostblogRaw):Postblog{  
    console.log('PostsData::',post);
    return {
      id:post.id,
      title:post.title,
      body:post.body    
      }
     }
   }