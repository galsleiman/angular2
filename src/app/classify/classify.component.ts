import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  favoriteCorporation: string;
  corporations:string[] = ['BBC', 'CNN', 'NBC'];
  
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.favoriteCorporation=this.route.snapshot.params.corporations;

}}
