import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage:string; 
  isError:boolean = false
  email:string;
  password:string;
 
  onSubmit(){
    this.auth.login(this.email,this.password).then(
      res =>{
        console.log('Succesful login;');
        this.router.navigate(['/books']); 
      }
    ).catch(
      err => {
        console.log(err);
        this.isError = true; 
        this.errorMessage = err.message; 
      } 
    ) 
  }
  
  constructor(private auth:AuthService,private router:Router) { }
  
  ngOnInit(): void {
  }

}
