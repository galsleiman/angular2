
import { AngularFireModule } from '@angular/fire';

import { AngularFireAuthModule } from '@angular/fire/auth';

import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BooksComponent } from './books/books.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatListModule } from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { ClassifyComponent } from './classify/classify.component';
import { FormsModule } from '@angular/forms';
import { CityFormComponent } from './city-form/city-form.component';
import {MatSelectModule} from '@angular/material/select';
import { PostsComponent } from './posts/posts.component';



import { LoginComponent } from './login/login.component';
import { environment } from 'src/environments/environment';
import { SignUpComponent } from './sign-up/sign-up.component';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { BookFormComponent } from './book-form/book-form.component';
import {MatInputModule} from '@angular/material/input';




@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    TemperaturesComponent,
    ClassifyComponent,
    CityFormComponent,
    PostsComponent,
    LoginComponent,
    SignUpComponent,
    BookFormComponent,


  ],
  imports: [
    FormsModule, 
    MatExpansionModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatCardModule,
    MatPaginatorModule,
    MatListModule,
    MatRadioModule,
    MatSelectModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
