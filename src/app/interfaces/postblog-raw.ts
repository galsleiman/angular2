export interface PostblogRaw {
        map(arg0: (post: any) => import("./postblog").Postblog): any;
    
        userId:number
        id:number
        title:string
        body:string
}

