import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { PostsComponent } from './posts/posts.component';
import { CityFormComponent } from './city-form/city-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';

import { ClassifyComponent } from './classify/classify.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';

const routes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'city', component:CityFormComponent },  
  { path: 'classify/:corporation', component: ClassifyComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'login', component: LoginComponent} ,
  { path: 'signup', component: SignUpComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
