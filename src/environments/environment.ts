// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDXRowPczQFijXJn13f9DfYoBs9C_FL67U",
    authDomain: "hello-gal.firebaseapp.com",
    databaseURL: "https://hello-gal.firebaseio.com",
    projectId: "hello-gal",
    storageBucket: "hello-gal.appspot.com",
    messagingSenderId: "511848976404",
    appId: "1:511848976404:web:9d2265884a1e36d5e09701"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
